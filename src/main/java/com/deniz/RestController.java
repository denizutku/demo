package com.deniz;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.deniz.models.Expense;
import com.deniz.models.Response;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    List<Expense> expenseList = new ArrayList<Expense>();

    @PostMapping(value = "/api/expense/save")
    public Response postCustomer(@RequestBody Expense expense, @AuthenticationPrincipal AtlassianHostUser hostUser) {
        expenseList.add(expense);
        Response response = new Response("Done", expense);
        return response;
    }

    @GetMapping(value = "/api/expense/all")
    public Response getResource(@AuthenticationPrincipal AtlassianHostUser hostUser) {
        Response response = new Response("Done", expenseList);
        return response;
    }
}

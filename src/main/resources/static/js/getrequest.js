$( document ).ready(function() {

    // GET REQUEST
    $("#getExpenses").click(function(event){
        event.preventDefault();
        ajaxGet();
    });

    // DO GET
    function ajaxGet(){
        $.ajax({
            type : "GET",
            url : "api/expense/all",
            headers: { 'Authorization': "JWT " + document.getElementById("token").value },
            success: function(result){
                if(result.status == "Done"){
                    $('#getResultDiv ul').empty();
                    var custList = "";
                    $.each(result.data, function(i, expense){
                        var expense = "- Expense with Id = " + i + ", description = " + expense.description + ", amount = " + expense.amount + "<br>";
                        $('#getResultDiv .list-group').append(expense)
                    });
                    console.log("Success: ", result);
                }else{
                    $("#getResultDiv").html("<strong>Error</strong>");
                    console.log("Fail: ", result);
                }
            },
            error : function(e) {
                $("#getResultDiv").html("<strong>Error</strong>");
                console.log("ERROR: ", e);
            }
        });
    }
})
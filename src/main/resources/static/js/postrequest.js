$( document ).ready(function() {

    // SUBMIT FORM
    $("#expenseForm").submit(function(event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        ajaxPost();
    });

    function ajaxPost(){

        // PREPARE FORM DATA
        var formData = {
            description : $("#description").val(),
            amount :  $("#amount").val()
        }

        // DO POST
        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/api/expense/save",
            data : JSON.stringify(formData),
            dataType : 'json',
            headers: { 'Authorization': "JWT " + document.getElementById("token").value },
            success : function(result) {
                if(result.status == "Done"){
                    $("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
                        "Post Successfully! <br>" +
                        "---> Expense's Info: Description = " +
                        result.data.description + " ,Amount = " + result.data.amount + "</p>");
                }else{
                    $("#postResultDiv").html("<strong>Error</strong>");
                }
                console.log(result);
            },
            error : function(e) {
                alert("Error!")
                console.log("ERROR: ", e);
            }
        });

        // Reset FormData after Posting
        resetData();

    }

    function resetData(){
        $("#description").val("");
        $("#amount").val("");
    }
})